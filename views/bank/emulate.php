<?php
/* @var $this yii\web\View */
use kartik\date\DatePicker;
?>
<container>
<h1>bank/Emulate</h1>
    <hr>


<?php     ?>


    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Cron Emulation</h3>
                <form>

                    <input type="hidden" name = "task" value="runEmulate">
                    <label>Specify the date you need, and submit the form to set incomes and charges for that day.</label>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?
                                $defaultStrDate = "";
                                echo DatePicker::widget([
                                    'name' => 'date',
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'value' => $defaultStrDate,
                                    'options' => ['placeholder' => 'Select date ...', 'readonly'=>'readonly'],
                                    'pluginOptions' => [
                                        'format' => 'dd-M-yyyy',
                                        'autoclose'=>true,
                                        'yearRange' => '2017:2030',
                                        'changeYear' => true,
                                        'todayHighlight' => true
                                    ]
                                ]);
                                echo '<br>';
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary emulateFormButton">Submit</button>
                        </div>
                    </div>

                    <div class="alert alert-danger alert-styled-left errorMsg" style=" display:none;  ">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Закрыть</span></button>
                        <span class="text-semibold"></span> <span class="msg">...</span></div>

                    <div class="alert alert-success alert-styled-left informMsg" style=" display:none; ">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Закрыть</span></button>
                        <span class="msg">...</span></div>

                </form>
            </div>
            <div class="col-md-6">

            </div>
        </div>
    </div>




</container>