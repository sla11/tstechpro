<?php

use yii\helpers\Html;
use app\components\helpers\DataFormatHelper;

?>

<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Month</th>
        <th scope="col">Profit</th>
        <th scope="col">Losses</th>
        <th scope="col">Value</th>
    </tr>
    </thead>
    <tbody>

    <? foreach($reportData as $year=>$yearRow):?>
        <? foreach($yearRow as $month=>$reportRow):?>
        <tr>
            <th scope="row"><?=(str_pad($month, 2, '0', STR_PAD_LEFT).'/'.$year)?></th>
            <td><?=DataFormatHelper::getDisplayMoney($reportRow['profit'], 2)?></td>
            <td><?=DataFormatHelper::getDisplayMoney($reportRow['losses'], 2)?></td>
            <td><?=DataFormatHelper::getDisplayMoney(($reportRow['profit']-$reportRow['losses']), 2)?></td>
        </tr>
        <? endforeach; ?>
    <? endforeach; ?>
    </tbody>
</table>


