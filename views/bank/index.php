<?php
/* @var $this yii\web\View */
?>
<h1>Reports</h1>


<div class="container">

    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_profit', compact('reportData') ) ?>
        </div>


    </div>

    <hr>
    <div class="row">

        <div class="col-md-12">
            <?= $this->render('_avg', compact('avgData')) ?>
        </div>

    </div>

</div>