<?php

use yii\helpers\Html;

use app\components\helpers\DataFormatHelper;

?>

<table class="table table-hover" style="max-width: 500px;">
    <thead>
    <tr>
        <th scope="col">Categories</th>
        <th scope="col">Average Deposit</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($avgData as $key=>$value):?>
    <tr>
        <th scope="row"><?=$key?></th>
        <td><?=DataFormatHelper::getDisplayMoney($value, 2)?></td>
    </tr>
    <?php endforeach; ?>

    </tbody>
</table>


