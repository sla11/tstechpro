<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChargesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Charges';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charges-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <p>
        <?= Html::a('Create Charges', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'account_id',
                        'date',
                        'amount_before',
                        'amount_after',
                        'value',
                        // 'dev_note',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <h4>Filter charges:</h4><hr>
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>
    </div>


</div>
