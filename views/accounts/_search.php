<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\AccountsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_id')->label('Bank client')
        ->dropDownList( $clientsList, ['prompt'=>'Select the client'] ); ?>

    <? // $form->field($model, 'percent') ?>

    <?
    $defaultStrDate = "";
    if($model->date) $defaultStrDate = date('d-M-Y', strtotime($model->date));
    echo '<label>Date created</label>';
    echo DatePicker::widget([
        'name' => 'AccountsSearch[date]',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => $defaultStrDate,
        'options' => ['placeholder' => 'Select date ...', 'readonly'=>'readonly'],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'autoclose'=>true,
            'yearRange' => '1930:2030',
            'changeYear' => true,
            'todayHighlight' => true
        ]
    ]);
    echo '<br>';
    ?>

    <? // $form->field($model, 'deposit') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
