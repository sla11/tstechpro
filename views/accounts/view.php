<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\components\helpers\DataFormatHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Accounts */

$this->title = $model->client->surname ." ". $model->client->name;
$this->params['breadcrumbs'][] = ['label' => 'Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accounts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'value' => '#'.$model->id,
                'label' => 'Account ID',
            ],
            [
                'attribute' => 'client_id',
                'value' => $model->client->surname ." ". $model->client->name. ", ID: ".$model->client_id,
                'label' => 'Client name',
            ],
            [
                'attribute' => 'percent',
                'value' => $model->percent.'%',
            ],
            [
                'attribute' => 'period',
                'value' => $model->period,
                'label' => 'Period (months)',
            ],
            [
                'attribute' => 'date',
                'value' => DataFormatHelper::getDisplayDate($model->date, 'datetime'),
                'label' => 'Created',
            ],
            [
                'attribute' => 'deposit',
                'value' => DataFormatHelper::getDisplayMoney($model->deposit, 2),
            ],
            [
                'attribute' => 'amount',
                'value' => DataFormatHelper::getDisplayMoney($model->amount, 2),
            ],
        ],
    ]) ?>


</div>
