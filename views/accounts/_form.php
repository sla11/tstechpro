<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Clients;


/* @var $this yii\web\View */
/* @var $model app\models\Accounts */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="accounts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'client_id')->label('Bank client')
        ->dropDownList( $clientsList, ['prompt'=>'Select the client'] ); ?>

    <?= $form->field($model, 'percent')->textInput(['value'=>($model->percent ? $model->percent : $model->getDefaults()->percent )]) ?>

    <?= $form->field($model, 'period')->textInput(['value'=>($model->period ? $model->period : $model->getDefaults()->period )]) ?>

    <?php if(!$model->isNewRecord): ?>
    <?php echo $form->field($model, 'date')->widget(
        'trntv\yii\datetime\DateTimeWidget',
        [
            'phpDatetimeFormat' => "dd.MM.yyyy, HH:mm:ss",
            'clientOptions' => [
                'defaultDate' => 'moment',
                'allowInputToggle' => true,
                'widgetPositioning' => [
                    'autoclose'=>true,
                    'ignoreReadonly' => true,
                ]
            ]
        ]
    );
    ?>
    <? endif; ?>

    <?= $form->field($model, 'deposit')->textInput(['maxlength' => true, 'value'=>($model->deposit ? $model->deposit : $model->getDefaults()->deposit  )] ) ?>

    <?php if($model->isNewRecord): ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <? endif; ?>

    <?php ActiveForm::end(); ?>

</div>
