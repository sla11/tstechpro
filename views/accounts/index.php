<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\helpers\DataFormatHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccountsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accounts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <p>
        <?= Html::a('Create New Account', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [

                        [
                            'attribute' => 'id',
                            'value' => 'id',
                            'label' => 'Account',
                        ],

                        [
                            'attribute' => 'client_id',
                            'value' => function($model, $key, $index, $widget) {
                                return DataFormatHelper::getDisplayFullName($model->client->name, $model->client->surname);
                            },
                            'label' => 'Client name',
                        ],

                        'percent',
                        'period',
                        [
                            'attribute' => 'date',
                            'value' => function($model, $key, $index, $widget) {
                                return DataFormatHelper::getDisplayDate($model->date, 'datetime');
                            }
                        ],

                        [
                            'attribute' => 'deposit',
                            'value' => function($model, $key, $index, $widget) {
                                return DataFormatHelper::getDisplayMoney($model->deposit);
                            }
                        ],

                        [
                            'attribute' => 'amount',
                            'value' => function($model, $key, $index, $widget) {
                                return DataFormatHelper::getDisplayMoney($model->amount);
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',

                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <h4>Filter accounts:</h4><hr>
                <?php  echo $this->render('_search', ['model' => $searchModel, 'clientsList'=>$clientsList]); ?>
            </div>
        </div>
    </div>



</div>
