<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Accounts */

$this->title = 'Update Accounts: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->client->surname . ' ' . $model->client->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="accounts-update">

    <h1><?= Html::encode('Update for '.$model->client->surname . ' ' . $model->client->name) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'clientsList'=>$clientsList
    ]) ?>

</div>
