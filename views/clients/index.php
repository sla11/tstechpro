<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\helpers\DataFormatHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Clients';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clients-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <hr>
    <p>
        <?= Html::a('Add New Client', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <hr>


    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [

                        'id',
                        'name',
                        'surname',
                        [
                            'attribute' => 'gender',
                            'value' => function($model, $key, $index, $widget) {
                                return DataFormatHelper::getDisplayGender($model->gender);
                            },
                        ],
                        [
                            'attribute' => 'birth',
                            'value' => function($model, $key, $index, $widget) {
                                return DataFormatHelper::getDisplayDate($model->birth, 'date');
                            }
                        ],

                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'buttons'=>[
                                'accounts'=>function ($url, $model) {
                                    $customurl=Yii::$app->getUrlManager()->createUrl(['accounts/index','AccountsSearch[client_id]'=>$model['id']]); //$model->id для AR
                                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-list"></span>', $customurl,
                                        ['title' => Yii::t('yii', 'Accounts'), 'data-pjax' => '0']);
                                }
                            ],
                            'template'=>'{accounts}',
                            'header' => 'Accounts'
                        ],

                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template'=>' {view}  {update}  {delete}',
                        ]

                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <h4>Filter clients:</h4><hr>
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>

        </div>

    </div>

</div>
