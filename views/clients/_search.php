<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsSearch */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="clients-search">


    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);

    ?>
    <?= $form->field($model, 'id') ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'surname') ?>

    <?php echo $form->field($model, 'gender')->dropDownList(['1' => 'Male', '2' => 'Female'],['prompt'=>'Select gender']); ?>



    <?php
    /*
    if($model->birth) $birthStamp = strtotime($model->birth);
        else $birthStamp = time();
    echo '<label>Birthday Date</label>';
    echo DatePicker::widget([
        'name' => 'ClientsSearch[birth]',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => date('d-M-Y', $birthStamp),
        'options' => ['placeholder' => 'Select birthday date ...', 'readonly'=>'readonly'],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'autoclose'=>true,
            'yearRange' => '1930:2030',
            'changeYear' => true,
            'todayHighlight' => true
        ]
    ]);
    echo '<br>';
    */
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
