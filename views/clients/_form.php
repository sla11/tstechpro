<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <? echo $form->field($model, 'gender')->dropDownList(['1' => 'Male', '2' => 'Female'],['prompt'=>'Select gender']); ?>

    <?

    if($model->birth) {
        $birthStr = date('d-M-Y', strtotime($model->birth));
        $model->birth = $birthStr;
    }

    echo $form->field($model, 'birth')->label('Birthday Date')->widget(
        'kartik\date\DatePicker',
        [
            'name' => 'Clients[birth]',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'options' => ['placeholder' => 'Select birthday date ...' ],//'readonly'=>'readonly'
            'pluginOptions' => [
                'format' => 'dd-M-yyyy',
                'autoclose'=>true,
                'yearRange' => '1930:2030',
                'changeYear' => true,
                'todayHighlight' => true
            ]
        ]
    );
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
