<?php
/**
 * Created by PhpStorm.
 * User: yaz
 * Date: 21.11.2017
 * Time: 12:15
 */

namespace app\logic\Project\Payment;

use app\models\Accounts;
use app\models\Charges;
use yii\db\Expression;
use Yii;


class ChargesMaker implements ITransaction {

    public function createTransaction($date = NULL){

        $nowDate = $date;
        if(empty($nowDate)) $nowDate = date('Y-m-d H:i:s');
        $currentDay = date('j',strtotime($nowDate));
        $currentMonth = date('n',strtotime($nowDate));
        $currentYear = date('Y',strtotime($nowDate));
        $currentDateTime = date('Y-m-d H:i:s',strtotime($nowDate));

        // last month data
        $lastMonthYear = $currentYear;
        $lastMonth = $currentMonth - 1;
        if($lastMonth == 0) {
            $lastMonth = 12;
            $lastMonthYear = $lastMonthYear -1;
        }
        $lastMonthStartStr = "{$lastMonthYear}-{$lastMonth}-01 00:00:01";
        $lastMonthEndStr = "{$lastMonthYear}-{$lastMonth}-31 23:59:59";

        // accounts which registered last month
        $lastMonthAccountsArray = Yii::$app->db
            ->createCommand('SELECT id FROM accounts WHERE date>:lastMonthStartStr AND date<:lastMonthEndStr ')
            ->bindValue(':lastMonthStartStr', $lastMonthStartStr)
            ->bindValue(':lastMonthEndStr', $lastMonthEndStr)
            ->queryColumn();

        // if 1st - make transaction
        if($currentDay == 1){
            $accounts = Accounts::find()->all();

            if(!empty($accounts)) {
                foreach ($accounts as $account) {

                    # for more security should check if transaction this month was already done
                    $isExist = Charges::find()->where("MONTH(`date`) = :monthNow AND YEAR(`date`) = :yearNow AND account_id=:account_id ", [":monthNow"=>$currentMonth, ":yearNow"=>$currentYear, ":account_id"=>$account->id])->limit(1)->one();
                    if($isExist){
                        continue;
                    }

                    $modelCharges = new Charges();
                    $modelCharges->account_id = $account->id;
                    $modelCharges->date = $currentDateTime;
                    $modelCharges->amount_before = $account->amount;

                    // count the payment value
                    if($account->amount > 0 && $account->amount < 1000){ //Комиссия 5%, но не менее чем 50 у.е.
                        $fee = 5;//%
                        $operand1 = bcmul ($account->amount, $fee, 4);//Умножение двух чисел с произвольной точностью
                        $payment = bcdiv ($operand1, 100, 4); // Операция деления для чисел произвольной точности
                        //$payment = (($account->amount * $fee) / 100);
                        if($payment < 50) $payment = 50;

                    }else if($account->amount >= 1000 && $account->amount < 10000){ // Комисcия 6%
                        $fee = 6;//%
                        $operand1 = bcmul ($account->amount, $fee, 4);//Умножение двух чисел
                        $payment = bcdiv ($operand1, 100, 4); // Операция деления
                        //$payment = (($account->amount * $fee) / 100);

                    }else if($account->amount >= 10000){ //Комиссия 7%, но не более чем 5000 у.е.
                        $fee = 7;//%
                        //$payment = (($account->amount * $fee) / 100);
                        $operand1 = bcmul ($account->amount, $fee, 4);//Умножение двух чисел
                        $payment = bcdiv ($operand1, 100, 4); // Операция деления
                        if($payment > 5000) $payment = 5000;
                    }

                    // if account created last month
                    if(in_array($account->id , $lastMonthAccountsArray)){
                        $accountDayCreated = date('j',strtotime($account->date));
                        $accountMonthDaysCount = date('t',strtotime($account->date));
                        $daysUsed = $accountMonthDaysCount - $accountDayCreated + 1;// how many days used

                        $paymentPerDay = bcdiv ($payment, $accountMonthDaysCount, 4); // fee for one day
                        $payment = bcmul ($daysUsed, $paymentPerDay, 4); // fee for used days
                        $modelCharges->dev_note = "Account created last month: {$daysUsed} days used";
                    }

                    // if account created today (1st)
                    if(date('Y-m-d',strtotime($nowDate)) == date('Y-m-d',strtotime($account->date))){
                        $payment = 0;
                        $modelCharges->dev_note = "Account created today: {$account->date}, $0 fee";
                    }

                    $modelCharges->value = $payment;
                    $modelCharges->amount_after = bcsub($account->amount, $payment, 4); //Вычитает одно число из другого с заданной точностью

                    //  break;

                    $modelCharges->save();

                    // update account record
                    $account->amount = $modelCharges->amount_after;
                    $account->save();
                }
            }

        }

        return true;
    }

} 