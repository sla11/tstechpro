<?php
/**
 * Created by PhpStorm.
 * User: yaz
 * Date: 21.11.2017
 * Time: 12:15
 */

namespace app\logic\Project\Payment;

use app\models\Accounts;
use app\models\Incomes;
use yii\db\Expression;


class IncomesMaker implements ITransaction {

    /**
     * Make transaction with specified date or today
     * @param null $date
     */
    public function createTransaction($date = NULL){

        $nowDate = $date;
        if(empty($nowDate)) $nowDate = date('Y-m-d H:i:s');;

        $currentDay = date('j',strtotime($nowDate));
        $currentDateTime = date('Y-m-d H:i:s',strtotime($nowDate));
        $currentMonth = date('n',strtotime($nowDate));
        $currentYear = date('Y',strtotime($nowDate));
        $lastDay = date('t',strtotime($nowDate));

        // if today the last month day
        if($currentDay == $lastDay){
            // get all where openDay>=nowDay
            $accounts = Accounts::find()->where(['>=', new Expression('DAY(date)'), $currentDay])->all();
        }else{
            // not last month day
            $accounts = Accounts::find()->where(['=', new Expression('DAY(date)'), $currentDay])->all();
        }

        if(!empty($accounts)){
            foreach($accounts as $account){

                # for more security should check if transaction this month was already done
                $isExist = Incomes::find()->where("MONTH(`date`) = :monthNow AND YEAR(`date`) = :yearNow AND account_id=:account_id ", [":monthNow"=>$currentMonth, ":yearNow"=>$currentYear, ":account_id"=>$account->id])->limit(1)->one();
                if($isExist){
                    continue;
                }

                $modelIncomes = new Incomes();
                $modelIncomes->account_id = $account->id;
                $modelIncomes->date = $currentDateTime;
                $modelIncomes->amount_before = $account->amount;

                if(empty($account->period)) $account->period = 1;

                $operand1 = bcmul ($account->amount, $account->percent, 4);//Умножение двух чисел с произвольной точностью
                $operand2 = bcdiv ($operand1, 100, 4); // Операция деления для чисел произвольной точности
                $payment = bcdiv ($operand2, $account->period, 4);

                $modelIncomes->value = $payment;
                $modelIncomes->amount_after = bcadd($account->amount, $payment, 4); //Сложить 2 числа произвольной точности

                if($currentDay == $lastDay)
                    $modelIncomes->dev_note = "AccCreated: {$account->date}, Today {$nowDate} the last month day";

                // update income record
                $modelIncomes->save();

                // update account record
                $account->amount = $modelIncomes->amount_after;
                $account->save();

               // break;
            }
        }
    }


} 