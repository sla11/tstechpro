<?php
/**
 * Created by PhpStorm.
 * User: yaz
 * Date: 21.11.2017
 * Time: 12:04
 */

namespace app\logic\Project\Payment;


interface ITransaction {

    public function createTransaction($date);

} 