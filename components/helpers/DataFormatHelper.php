<?php
/**
 * Created by PhpStorm.
 * User: yaz
 * Date: 19.11.2017
 * Time: 13:07
 */

namespace app\components\helpers;

class DataFormatHelper {

    /**
     * Display formatted money value for tables and grids
     * @param $value
     * @param int $decimals
     * @param string $currency_prefix
     * @param string $currency_sufix
     * @return string
     */
    public static function getDisplayMoney($value, $decimals = 0, $currency_prefix = '$', $currency_sufix = '' )
    {
        //return $currency_prefix.Yii::$app->formatter->format($value, ['decimal',2]).$currency_sufix;
        return $currency_prefix.number_format($value, $decimals, '.', ' ').$currency_sufix;
    }

    /**
     * Display formatted Client Name value for tables and grids
     * @param $name
     * @param $surname
     * @return string
     */
    public static function getDisplayFullName($name, $surname)
    {
        return $surname." ".$name;
    }

    /**
     * Display formatted Gender Label for tables and grids
     * @param $gender_id
     * @return string
     */
    public static function getDisplayGender($gender_id)
    {

        switch ($gender_id) {
            case 1:
                $res = 'Male';
                break;
            case 2:
                $res = 'Female';
                break;
            default:
                $res = 'Undefined';
        }

        return $res;
    }

    /**
     * Display formatted date value for tables and grids
     * @param $dateStr
     * @param string $type  date | datetime | time | mysql
     * @return string
     */
    public static function getDisplayDate($dateStr, $type = "date")
    {
        $date = new \DateTime($dateStr);

        switch ($type) {
            case 'date':
                $res = $date->format('M j, Y');
                break;
            case 'datetime':
                $res = $date->format('M j, Y H:i:s');
                break;
            case 'time':
                $res = $date->format('H:i:s');
                break;
            case 'mysql':
                $res = $date->format('Y-m-d H:i:s');
                break;
            default:
                $res = $date->format('Y-m-d H:i:s');
        }

        return $res;
    }
} 