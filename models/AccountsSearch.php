<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accounts;

/**
 * AccountsSearch represents the model behind the search form about `app\models\Accounts`.
 */
class AccountsSearch extends Accounts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'percent'], 'integer'],
            [['date'], 'safe'],
            [['deposit', 'amount', 'period'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accounts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'percent' => $this->percent,
            'deposit' => $this->deposit,
            'amount' => $this->amount,
            'period' => $this->period,
        ]);

        if($this->date){
            $mysqlTimeStart = date('Y-m-d 00:00:00', strtotime($this->date));
            $mysqlTimeEnd = date('Y-m-d 23:59:59', strtotime($this->date));
            $query->andFilterWhere(['>', 'date', $mysqlTimeStart]);
            $query->andFilterWhere(['<', 'date', $mysqlTimeEnd]);
        }

        return $dataProvider;
    }
}
