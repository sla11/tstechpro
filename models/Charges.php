<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charges".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $date
 * @property string $amount_before
 * @property string $amount_after
 * @property string $value
 * @property string $dev_note
 */
class Charges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'charges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'integer'],
            [['date'], 'safe'],
            [['amount_before', 'amount_after', 'value'], 'number'],
            [['dev_note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'date' => 'Date',
            'amount_before' => 'Amount Before',
            'amount_after' => 'Amount After',
            'value' => 'Value',
            'dev_note' => 'Dev Note',
        ];
    }
}
