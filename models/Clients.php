<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property integer $gender
 * @property string $birth
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'gender', 'birth'], 'required'],
            [['gender'], 'integer'],
            [['birth'], 'safe'],
            [['name', 'surname'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'gender' => 'Gender',
            'birth' => 'Birth',
        ];
    }

    /**
     * Get Client accounts
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts(){
        return $this->hasMany(Accounts::className(), ['client_id'=>'id']);
    }

    /**
     * Using in dropdown List
     */
    public function getList(){

        $userList = ArrayHelper::map($this->find('id', 'CONCAT(name, surname) AS fullName')
            ->select(['id', new \yii\db\Expression("CONCAT( `surname`, ' ', `name`) as name")] )
            ->all(), 'id', 'name');

        return $userList;
    }

}
