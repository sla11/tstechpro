<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $percent
 * @property string $date
 * @property string $deposit
 * @property string $amount
 * @property string $period
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'percent', 'deposit', 'period'], 'required'],
            [['client_id', 'percent', 'period'], 'integer'],
            [['date'], 'safe'],
            [['deposit', 'amount'], 'number'],
        ];
    }


    /**
     * To set the default form values
     * @return array
     */
    public function getDefaults()
    {
        return (object)[
            'percent' => '26',
            'deposit' => '1001',
            'period' => '12',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'percent' => 'Percent (%)',
            'date' => 'Date',
            'deposit' => 'Deposit',
            'amount' => 'Amount',
            'period' => 'Period',
        ];
    }

    public function getClient(){
        return $this->hasOne(Clients::className(), ['id'=>'client_id']);
    }

}
