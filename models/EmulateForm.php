<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class EmulateForm extends Model
{
    public $date;
    //public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Emulation date',
        ];
    }

    public function test()
    {
        echo "";
        return false;
    }
}
