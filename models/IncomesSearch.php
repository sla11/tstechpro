<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Incomes;

/**
 * IncomesSearch represents the model behind the search form about `app\models\Incomes`.
 */
class IncomesSearch extends Incomes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'account_id'], 'integer'],
            [['date', 'dev_note'], 'safe'],
            [['amount_before', 'amount_after', 'value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Incomes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'account_id' => $this->account_id,
            'amount_before' => $this->amount_before,
            'amount_after' => $this->amount_after,
            'value' => $this->value,
        ]);

        if($this->date){
            $mysqlTimeStart = date('Y-m-d 00:00:00', strtotime($this->date));
            $mysqlTimeEnd = date('Y-m-d 23:59:59', strtotime($this->date));
            $query->andFilterWhere(['>=', 'date', $mysqlTimeStart]);
            $query->andFilterWhere(['<', 'date', $mysqlTimeEnd]);
        }

        $query->andFilterWhere(['like', 'dev_note', $this->dev_note]);

        return $dataProvider;
    }
}
