<?php

namespace app\models;

use Yii;
use app\models\Accounts;
use yii\db\Expression;

/**
 * This is the model class for table "incomes".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $date
 * @property string $amount_before
 * @property string $amount_after
 * @property string $value
 * @property string $dev_note
 */
class Incomes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incomes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'integer'],
            [['date'], 'safe'],
            [['amount_before', 'amount_after', 'value'], 'number'],
            [['dev_note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'date' => 'Date',
            'amount_before' => 'Amount Before',
            'amount_after' => 'Amount After',
            'value' => 'Value',
            'dev_note' => 'Dev Note',
        ];
    }


    /**
     * Make transaction with specified date
     * @param $nowDate
     */
    public static function createTransaction($nowDate){

        $currentDay = date('j',strtotime($nowDate));
        $currentDateTime = date('Y-m-d H:i:s',strtotime($nowDate));
        $currentMonth = date('n',strtotime($nowDate));
        $lastDay = date('t',strtotime($nowDate));

        // if today the last month day
        if($currentDay == $lastDay){

            // get all which has openDay>=nowDay  =  nowDay<=openDay
            $accounts = Accounts::find()->where(['>=', new Expression('DAY(date)'), $currentDay])->all();
        }else{
            // not last month day
            $accounts = Accounts::find()->where(['=', new Expression('DAY(date)'), $currentDay])->all();
        }

        if(!empty($accounts)){
            foreach($accounts as $account){

                # for more security should check if transaction this month was already done
                $isExist = Incomes::find()->where("MONTH(`date`) = MONTH(NOW()) AND YEAR(`date`) = YEAR(NOW()) AND account_id=:account_id ", [":account_id"=>$account->id])->limit(1)->one();
                if($isExist){
                   // continue;
                }

                $modelIncomes = new Incomes();
                $modelIncomes->account_id = $account->id;
                $modelIncomes->date = $currentDateTime;
                $modelIncomes->amount_before = $account->amount;

                $payment = (($account->amount * $account->percent) / 100) / 12;
                $modelIncomes->value = $payment;
                $modelIncomes->amount_after = $account->amount + $payment;

                if($currentDay == $lastDay)
                    $modelIncomes->dev_note = "accCreated: {$account->date}, Today {$nowDate} the last month day";

                // update income record
                $modelIncomes->save();

                // update account record
                $account->amount = $modelIncomes->amount_after;
                $account->save();

                break;
            }
        }

    }

}