<?php

namespace app\controllers;

use app\models\Clients;
use Yii;
use app\models\Accounts;
use app\models\AccountsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountsController implements the CRUD actions for Accounts model.
 */
class AccountsController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {

        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','create','update','view'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all Accounts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AccountsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelClient = new Clients();
        $clientsList = $modelClient->getList();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'clientsList'=>$clientsList
        ]);
    }

    /**
     * Displays a single Accounts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Accounts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Accounts();

        if ($model->load(Yii::$app->request->post()) ) {

            if($model->validate()){

                $model->amount = $model->deposit;
                if($model->date) $model->date = date('Y-m-d H:i:s', strtotime($model->date));
                    else $model->date = date('Y-m-d H:i:s');

                if($model->save()){
                    Yii::$app->session->setFlash('success', 'Account successfully created');
                    return $this->redirect(['view', 'id' => $model->id]);
                    //return $this->refresh();// перезагрузка и очистка формы
                }else{
                    Yii::$app->session->setFlash('error', 'Error saving new account: '.$model->getErrors());
                }

            }else{
                Yii::$app->session->setFlash('danger', 'Error data validation');
            }

        } else {

            $modelClient = new Clients();
            $clientsList = $modelClient->getList();

            return $this->render('create', [
                'model' => $model, 'clientsList'=>$clientsList
            ]);
        }
    }

    /**
     * Updates an existing Accounts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        Yii::$app->session->setFlash('danger', 'Editing is forbidden.');
        //if ($model->load(Yii::$app->request->post())) {
            //if($model->save()) return $this->redirect(['view', 'id' => $model->id]);
        //}

            $modelClient = new Clients();
            $clientsList = $modelClient->getList();

            return $this->render('update', [
                'model' => $model, 'clientsList'=>$clientsList
            ]);
    }

    /**
     * Deletes an existing Accounts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->session->setFlash('danger', 'Deleting is forbidden.');

        //$this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Accounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Accounts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
