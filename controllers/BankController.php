<?php

namespace app\controllers;

use app\models\Accounts;
use app\models\Charges;
use app\models\Clients;
use app\models\Incomes;
use app\logic\Project\Payment;
use Yii;
use app\models\LoginForm;
use yii\filters\VerbFilter;
use Faker\Factory;

class BankController extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */

    public function behaviors()
    {

        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','create','update','view'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

        $connection = Yii::$app->db;
        $reportModel = $connection->createCommand('SELECT YEAR(date) as year, MONTH(date) as month, SUM(value) as s
        FROM incomes GROUP BY year, month order BY year DESC, month DESC')
            ->queryAll();

        $reportData = [];

        if($reportModel){
            foreach ($reportModel as $report){
                $reportData[$report['year']][$report['month']]['losses'] = $report['s'];
            }
        }

        $reportModel = $connection->createCommand('SELECT YEAR(date) as year, MONTH(date) as month, SUM(value) as s
        FROM charges GROUP BY year, month order BY year DESC, month DESC')
            ->queryAll();

        if($reportModel){
            foreach ($reportModel as $report){
                $reportData[$report['year']][$report['month']]['profit'] = $report['s'];
            }
        }

        // =================

        $avgData = [];
        $avgCat1 = Yii::$app->db
            ->createCommand('SELECT AVG(acc.deposit) val FROM accounts as acc
              JOIN clients as client ON client.id = acc.client_id
              WHERE client.birth > (DATE_SUB(CURDATE(), INTERVAL 25 YEAR)) AND
              client.birth <= (DATE_SUB(CURDATE(), INTERVAL 18 YEAR)) ')
            ->queryOne();
        $avgData['18-25'] = $avgCat1['val'];

        $avgCat2 = Yii::$app->db
            ->createCommand('SELECT AVG(acc.deposit) val  FROM accounts as acc
              JOIN clients as client ON client.id = acc.client_id
              WHERE client.birth > (DATE_SUB(CURDATE(), INTERVAL 50 YEAR)) AND
              client.birth <= (DATE_SUB(CURDATE(), INTERVAL 25 YEAR)) ')
            ->queryOne();
        $avgData['25-50'] = $avgCat2['val'];

        $avgCat3 = Yii::$app->db
            ->createCommand('SELECT AVG(acc.deposit) val FROM accounts as acc
              JOIN clients as client ON client.id = acc.client_id
              WHERE client.birth <= (DATE_SUB(CURDATE(), INTERVAL 50 YEAR)) ')
            ->queryOne();
        $avgData['more then 50'] = $avgCat3['val'];

        return $this->render('index', compact('reportData', 'avgData') );
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Data generator for Clients and their accounts
     */
    public function actionDataGenerator(){

        $lock = true;
        if($lock) return "actionDataGenerator locked";

        $clientCount = 500;
        $clientAccCount = rand(1, 4);
        $faker = Factory::create();

       for($i=1; $i<=$clientCount; $i++){

           $from = strtotime("1950-01-01 00:00:00");
           $to = strtotime("2000-01-01 00:00:00");
           $clientBirthDateStamp = mt_rand($from, $to);
           $clientBirthDateStr = date("Y-m-d H:i:s",$clientBirthDateStamp);
           $clientGender = rand(1, 2);

           $modelClient = new Clients();
           $modelClient->name = $faker->firstName;
           $modelClient->surname = $faker->firstName;
           $modelClient->gender = $clientGender;
           $modelClient->birth = $clientBirthDateStr;
           if($modelClient->save()){
                if($modelClient->id){
                    for($j=1; $j<=$clientAccCount; $j++){

                        $now = time();
                        $from = strtotime("2016-01-01 00:00:00");
                        $accountDateStamp = mt_rand($from, $now);
                        $accountDateStr = date("Y-m-d H:i:s",$accountDateStamp);
                        $accountPercent = mt_rand(10, 40);
                        $accountDeposit = round(mt_rand(2000, 300000), -2);

                        $modelAccount = new Accounts();
                        $modelAccount->client_id = $modelClient->id;
                        $modelAccount->percent = $accountPercent;
                        $modelAccount->date = $accountDateStr;
                        $modelAccount->deposit = $accountDeposit;
                        $modelAccount->amount = $accountDeposit;
                        $modelAccount->save();
                    }
                }
            }

       }

        dump('Data Generation done.');

    }

    /**
     * Ajax for starting transaction by specified date
     * @return string
     */
    public function actionEmulate()
    {
        if(Yii::$app->request->isAjax){
            $ajaxData = Yii::$app->request->post();

            if(empty($ajaxData['date'])){

                return json_encode(array('success' => false, 'info' => 'Date can not be empty'));

            } else if($ajaxData['date']){

                $dateStamp = strtotime($ajaxData['date']);
                $dateStr = date('Y-m-d H:i:s', $dateStamp);
                $dateNowStr = date('Y-m-d 00:00:00');

                if($dateStr<$dateNowStr){
                    //return json_encode(array('success' => false, 'info' => "Date too old"));
                }
            }

            $IncomesMaker = new Payment\IncomesMaker();
            $IncomesMaker ->createTransaction($dateStr);

            $ChargesMaker = new Payment\ChargesMaker();
            $ChargesMaker ->createTransaction($dateStr);

            $res = json_encode(array('success' => true, 'noScroll'=>true, 'hideForm'=>false, 'reload' => false, 'info' => "Success!", 'redirectURL' => ""));
            return $res;
        }

        return $this->render('emulate', []);
    }

    /**
     * Can be set as crone
     */
    public function actionCron(){
        $IncomesMaker = new Payment\IncomesMaker();
        $IncomesMaker ->createTransaction();
        echo "IncomesMaker done; <br>";
        $ChargesMaker = new Payment\ChargesMaker();
        $ChargesMaker ->createTransaction();
        echo "ChargesMaker done; <br>";
    }

}
