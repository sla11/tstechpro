
$(document).ready(function() {

    /**
     * Аяксовые формы
     */
    $( "body" ).on( "click", ".emulateFormButton", function( e ) {
        e.preventDefault();  // Полная остановка происходящего
        var stopAll = false;

        var form = $(e.target).closest('form');
        var task = form.find("input[name=task]").val();

        if(task=='runEmulate') ajaxUrl = "/bank/emulate";
        var buttonObj =  $(this);
        //var spinnerClass = "icon-spinner9";

        var msgBlock = form;
        var formData = new FormData($(this).closest('form')[0]);

        if(!stopAll){
            stopAll = true;
            buttonObj.prop("disabled", true);
          //  buttonObj.find('i').addClass(spinnerClass).addClass('spinner');

            msgBlock.find('.errorMsg').hide();
            msgBlock.find('.informMsg').hide();


            $.ajax({
                url: ajaxUrl,
                type: 'POST',
                data: formData,
                cache: false,
                dataType: 'json',
                processData: false, // Не обрабатываем файлы (Don't process the files)
                contentType: false, // Так jQuery скажет серверу что это строковой запрос
                success: function( response, textStatus, jqXHR ){

                    // если есть какой-либо ответ прячем блоки
                    msgBlock.find('.errorMsg').hide();
                    msgBlock.find('.informMsg').hide();

                    // Если все ОК
                    if(response.success==true){// Файлы успешно загружены
                        console.log("true");
                        msgBlock.find('.informMsg span.msg').html(response.info);
                        msgBlock.find('.informMsg').show();

                        if(response.hideForm==true) {
                            form.find('.formFields').hide(); // прячем поля формы
                            buttonObj.hide(); // прячем кнопку
                        }

                        if(response.reload==true){
                            location.reload();
                        }else if(typeof response.goto !== 'undefined'){
                            setTimeout( 'location="'+response.goto+'";', 2000 );
                        }else if(response.noScroll!=true){
                            $('html, body').animate({scrollTop: 0}, 500);
                        }
                    }
                    else{
                        console.log("false");
                        msgBlock.find('.errorMsg span.msg').html(response.info);
                        msgBlock.find('.errorMsg').show();
                    }

                    // включим кнопку
                    buttonObj.removeAttr('disabled');
                   // buttonObj.find('i').removeClass(spinnerClass).removeClass('spinner');

                },
                error: function( jqXHR, textStatus, errorThrown ){
                    console.log("error");
                    console.log(jqXHR);
                    buttonObj.removeAttr('disabled');
                    //buttonObj.find('i').removeClass(spinnerClass).removeClass('spinner');
                    msgBlock.find('.errorMsg span.msg').html("Ошибка отправки запроса");
                    msgBlock.find('.errorMsg').show();
                    msgBlock.find('.informMsg').hide();
                }
            });
        }
    });

});